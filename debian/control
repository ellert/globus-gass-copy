Source: globus-gass-copy
Priority: optional
Maintainer: Mattias Ellert <mattias.ellert@physics.uu.se>
Build-Depends:
 debhelper-compat (= 13),
 dpkg-dev (>= 1.22.5),
 pkgconf,
 libglobus-common-dev (>= 15),
 libglobus-ftp-client-dev (>= 7),
 libglobus-ftp-control-dev (>= 4),
 libglobus-gsi-sysconfig-dev (>= 4),
 libglobus-gass-transfer-dev (>= 7),
 libglobus-io-dev (>= 8),
 libglobus-gssapi-gsi-dev (>= 9),
 libglobus-gssapi-error-dev (>= 4),
 libssl-dev,
 libglobus-gridftp-server-dev (>= 7),
 globus-gridftp-server-progs (>= 7),
 liburi-perl,
 openssl
Build-Depends-Indep:
 doxygen
Standards-Version: 4.6.2
Section: net
Vcs-Browser: https://salsa.debian.org/ellert/globus-gass-copy
Vcs-Git: https://salsa.debian.org/ellert/globus-gass-copy.git
Homepage: https://github.com/gridcf/gct/

Package: libglobus-gass-copy2t64
Provides: ${t64:Provides}
Replaces: libglobus-gass-copy2
Breaks: libglobus-gass-copy2 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Grid Community Toolkit - Globus Gass Copy
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The libglobus-gass-copy2t64 package contains:
 Globus Gass Copy

Package: globus-gass-copy-progs
Architecture: any
Multi-Arch: foreign
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Grid Community Toolkit - Globus Gass Copy Programs
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The globus-gass-copy-progs package contains:
 Globus Gass Copy Programs

Package: libglobus-gass-copy-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libglobus-gass-copy2t64 (= ${binary:Version}),
 ${misc:Depends},
 libglobus-common-dev (>= 15),
 libglobus-ftp-client-dev (>= 7),
 libglobus-ftp-control-dev (>= 4),
 libglobus-gsi-sysconfig-dev (>= 4),
 libglobus-gass-transfer-dev (>= 7),
 libglobus-io-dev (>= 8),
 libglobus-gssapi-gsi-dev (>= 9),
 libglobus-gssapi-error-dev (>= 4),
 libssl-dev
Suggests:
 libglobus-gass-copy-doc (= ${source:Version})
Description: Grid Community Toolkit - Globus Gass Copy Development Files
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The libglobus-gass-copy-dev package contains:
 Globus Gass Copy Development Files

Package: libglobus-gass-copy-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends}
Description: Grid Community Toolkit - Globus Gass Copy Documentation Files
 The Grid Community Toolkit (GCT) is an open source software toolkit used for
 building grid systems and applications. It is a fork of the Globus Toolkit
 originally created by the Globus Alliance. It is supported by the Grid
 Community Forum (GridCF) that provides community-based support for core
 software packages in grid computing.
 .
 The libglobus-gass-copy-doc package contains:
 Globus Gass Copy Documentation Files
